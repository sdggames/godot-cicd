# Docker images for Godot CI and CD.
This repository is used to build and maintain Docker images for my Godot [game template](https://gitlab.com/sdggames/template).

This is loosely based on [Barichello's](https://gitlab.com/barichello/godot-ci) [CI image](https://hub.docker.com/r/barichello/godot-ci/), but fixes support for [GDUnit4](https://github.com/MikeSchulze/gdUnit4).
It also omits the Java-SDK and Android-SDK, instead focusing on Windows, Linux, Mac, and Web builds. The final result is a much smaller image that works better for my PC and Web based projects.

#### Personal note: How to update to the next version of Godot (because I WILL forget.)
To update the latest version of Godot, start the build-and-push job with a custom variable. For the variables, set Key=VERSION, value=4.X.X
To run with variables, click the arrow under the rerun option, then select Update CI/CD variables.

# The Docker image is broken into four different images of varying sizes:
When using Gitlab's shared runners, using the proper image for each job should result in a much faster build, since the largest image doesn't need to be downloaded each time.

## Godot-base
This image takes Ubuntu and adds the Godot program. This is very small isn't very useful on its own.
This image does contain the Butler tool for pushing builds to Itch.io.

## Godot-test
This image takes Godot-base and adds X11. This enables GdUnit to run in the cloud.

## Godot-ci
This image takes Godot-base and installs Godot's export templates. This enables Godot to export/compile in the cloud.
The export templates only work for Windows, Linux, MacOS, and Web export. The Andriod SDK is not included.

## Godot-cicd
This image takes Godot-test, and adds the export templates from Godot-ci. This image is much larger than the others, but it does everything, so use it if space/download time is not a consideration.

## TODO: Godot-mobile
Someday, I might care about making games for phones, so there will be an image to do that. Check out Barichello's image if you need this support.