ARG RELEASE_NAME="stable"
ARG VERSION
ARG SUBDIR=""

# Image one: Godot on Ubuntu. Missing export templates and display drivers.
# Use an Ubuntu base image
FROM ubuntu:24.04 AS godot-base
ARG VERSION

# Install dependencies
RUN apt-get update && apt-get install -y --no-install-recommends \
    wget \
    unzip \
    xvfb \
    libfontconfig1 \
    ca-certificates \
    && update-ca-certificates \
    && rm -rf /var/lib/apt/lists/*

ENV GODOT_HOME="/usr/local/bin"
ENV GODOT_BIN="$GODOT_HOME/godot"
ENV GODOT_VERSION=$VERSION

# Create directories
RUN mkdir -p $GODOT_HOME & echo "Godot version is ${GODOT_VERSION}"

# Download and install Godot
RUN wget -q https://github.com/godotengine/godot/releases/download/${GODOT_VERSION}-stable/Godot_v${GODOT_VERSION}-stable_linux.x86_64.zip -P /tmp \
    && unzip -q /tmp/Godot_v${GODOT_VERSION}-stable_linux.x86_64.zip -d $GODOT_HOME \
    && mv $GODOT_HOME/Godot_v${GODOT_VERSION}-stable_linux.x86_64 $GODOT_BIN \
    && chmod u+x $GODOT_BIN \
    && rm /tmp/Godot_v${GODOT_VERSION}-stable_linux.x86_64.zip

# This is only a few bytes, so include it in the base image.
ADD getbutler.sh /opt/butler/getbutler.sh
RUN bash /opt/butler/getbutler.sh
RUN /opt/butler/bin/butler -V

# Make sure to run this at least once so basic configs are created.
RUN godot -v -e --quit --headless

ENV PATH="/opt/butler/bin:${PATH}"

# Image two - Install X11. Viable for unit testing.
FROM godot-base AS godot-test

# Install X11 display drivers to enable unit tests.
RUN apt-get update && apt-get install -y --no-install-recommends \
    libx11-dev \
    libxcursor-dev \
    libxinerama-dev \
    libxrandr-dev \
    libxi-dev \
    libglu1-mesa-dev \
    && rm -rf /var/lib/apt/lists/*

# This is not automatically defined for later images.
ENV PATH="/opt/butler/bin:${PATH}"


# Image three - Build and push. Tests will fail, but headless builds will pass.
FROM godot-base AS godot-ci
ARG RELEASE_NAME
ARG VERSION
ARG SUBDIR

RUN apt-get update && apt-get install -y --install-recommends \
    wine \
    wine64 \
    wine64-tools \
    osslsigncode \
    && ln -s /usr/lib/wine/wine64 /usr/bin/wine64 \
    && ln -s /usr/lib/wine/wineserver64 /usr/bin/wineserver64 \
    && rm -rf /var/lib/apt/lists/*

# Get Godot export templates.
RUN wget -q https://github.com/godotengine/godot/releases/download/${GODOT_VERSION}-${RELEASE_NAME}/Godot_v${GODOT_VERSION}-${RELEASE_NAME}_export_templates.tpz \
    && mkdir -p ~/.local/share/godot/export_templates/${GODOT_VERSION}.${RELEASE_NAME} \
    && ln -s ~/.local/share/godot/templates ~/.local/share/godot/export_templates \
    && unzip -q Godot_v${GODOT_VERSION}-${RELEASE_NAME}_export_templates.tpz \
    && mv templates/* ~/.local/share/godot/export_templates/${GODOT_VERSION}.${RELEASE_NAME} \
    && rm -f Godot_v${GODOT_VERSION}-${RELEASE_NAME}_export_templates.tpz

RUN wget https://github.com/electron/rcedit/releases/download/v2.0.0/rcedit-x64.exe -O /opt/rcedit.exe \
    && echo 'export/windows/rcedit = "/opt/rcedit.exe"' >> ~/.config/godot/editor_settings-4.3.tres \
    && echo 'export/windows/wine = "/usr/bin/wine64"' >> ~/.config/godot/editor_settings-4.3.tres

# This is not automatically defined for later images.
ENV PATH="/opt/butler/bin:${PATH}"

# Image four - Build, test, and push. This is the biggest image, so using build or test is preferred.
FROM godot-test AS godot-cicd
ARG RELEASE_NAME
ARG VERSION

RUN apt-get update && apt-get install -y --install-recommends \
    wine \
    wine64 \
    wine64-tools \
    osslsigncode \
    && ln -s /usr/lib/wine/wine64 /usr/bin/wine64 \
    && ln -s /usr/lib/wine/wineserver64 /usr/bin/wineserver64 \
    && rm -rf /var/lib/apt/lists/*

# Copy the export templates from the godot-ci stage
COPY --from=godot-ci /root/.local/share/godot/export_templates/${GODOT_VERSION}.${RELEASE_NAME} /root/.local/share/godot/export_templates/${GODOT_VERSION}.${RELEASE_NAME}

RUN wget -q https://github.com/electron/rcedit/releases/download/v2.0.0/rcedit-x64.exe -O /opt/rcedit.exe \
    && echo 'export/windows/rcedit = "/opt/rcedit.exe"' >> ~/.config/godot/editor_settings-4.3.tres \
    && echo 'export/windows/wine = "/usr/bin/wine64"' >> ~/.config/godot/editor_settings-4.3.tres

# This is not automatically defined for later images.
ENV PATH="/opt/butler/bin:${PATH}"